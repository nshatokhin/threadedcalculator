TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    types.cpp \
    calculator.cpp

HEADERS += \
    types.h \
    calculator.h

LIBS += -pthread
