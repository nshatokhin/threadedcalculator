#include "types.h"

Element::Element(const Types &type) : type_(type), value_(nullptr)
{

}

Element::~Element()
{

}

Types Element::type() const
{
    return type_;
}

void Element::setType(const Types &type)
{
    type_ = type;
}

Error::Error(const Errors &value)
{
    type_ = Types::ERROR;
    value_ = static_cast<void*>(new Errors(value));
}

Error::~Error()
{
    if(value_ != nullptr)
        delete static_cast<Errors*>(value_);
}

Errors Error::getValue() const
{
    return *getValuePtr();
}

void Error::setValue(int value)
{
    setValue(static_cast<void*>(&value));
}

Errors* Error::getValuePtr() const
{
    return static_cast<Errors*>(value_);
}

void * Error::value() const
{
    return value_;
}

void Error::setValue(void * value)
{
    memcpy(value_, value, sizeof(Errors));
}

std::string Error::toString() const
{
    return errToString_.at(getValue());
}

const ErrToString Error::errToString_({
    {Errors::DIVBYZERO, "DIVBYZERO"},
    {Errors::ZEROINZEROPOW, "ZEROINZEROPOW"},
    {Errors::INVEXPR, "INVEXPR"},
    {Errors::INVOPER, "INVOPER"}
});

Operation::Operation(const Operations &value)
{
    type_ = Types::OPERATION;
    value_ = static_cast<void*>(new Operations(value));
}

Operation::Operation(const char &value) : Operation(Operation::castOperation(value))
{}

Operation:: ~Operation()
{
    if(value_ != nullptr)
        delete static_cast<Operations*>(value_);
}

const OpPrio Operation::prioTable()
{
    return prioTable_;
}
const OpCaster Operation::operationCaster()
{
    return operationCaster_;
}

bool Operation::isOperation(const char &character)
{
    return Operation::operationCaster_.find(character) != Operation::operationCaster_.end();
}

Operation Operation::castOperation(const char &character)
{
    return Operation::operationCaster_.at(character);
}

int Operation::prio(const Operations& op)
{
    return prioTable_.find(op)->second;
}

Operations Operation::getValue() const
{
    return *getValuePtr();
}

Operations* Operation::getValuePtr() const
{
    return static_cast<Operations*>(value_);
}

void * Operation::value() const
{
    return value_;
}

void Operation::setValue(void * value)
{
    memcpy(value_, value, sizeof(Operations));
}

int Operation::prio() const
{
    return Operation::prioTable().at(this->getValue());
}


const OpPrio Operation::prioTable_({
    {Operations::ADD, 0},
    {Operations::SUB, 0},
    {Operations::DIV, 1},
    {Operations::MUL, 1},
    {Operations::POW, 2},
    {Operations::LPARENTHESIS, 0},
    {Operations::RPARENTHESIS, 0}
});


const OpCaster Operation::operationCaster_({
    {'+', Operations::ADD},
    {'-', Operations::SUB},
    {'/', Operations::DIV},
    {'*', Operations::MUL},
    {'^', Operations::POW},
    {'(', Operations::LPARENTHESIS},
    {')', Operations::RPARENTHESIS}
});

Integer::Integer(const int &value)
{
    type_ = Types::INTEGER;
    value_ = static_cast<void*>(new int(value));
}

Integer::~Integer()
{
    if(value_ != nullptr)
        delete static_cast<int*>(value_);
}

int Integer::getValue() const
{
    return *getValuePtr();
}

void Integer::setValue(int value)
{
    setValue(static_cast<void*>(&value));
}

int* Integer::getValuePtr() const
{
    return static_cast<int*>(value_);
}

void * Integer::value() const
{
    return value_;
}

void Integer::setValue(void * value)
{
    memcpy(value_, value, sizeof(int));
}


Double::Double(const double &value)
{
    type_ = Types::DOUBLE;
    value_ = static_cast<void*>(new double(value));
}

Double::~Double()
{
    if(value_ != nullptr)
        delete static_cast<double*>(value_);
}

double Double::getValue() const
{
    return *getValuePtr();
}

void Double::setValue(double value)
{
    setValue(static_cast<void*>(&value));
}

double* Double::getValuePtr() const
{
    return static_cast<double*>(value_);
}

void * Double::value() const
{
    return value_;
}

void Double::setValue(void * value)
{
    memcpy(value_, value, sizeof(double));
}
