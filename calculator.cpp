#include "calculator.h"

#include <iostream>
#include <map>
#include <cmath>

#define DOT '.'
#define COMMA ','
#define LEFT_PARENTHESIS '('
#define RIGHT_PARENTHESIS ')'

Calculator::Calculator()
{

}

void Calculator::processNumber()
{
    if(isNumber) {
        if(isReal) {
            outputStack.push_back(std::make_shared<Double>(stod(number)));
        } else {
            outputStack.push_back(std::make_shared<Integer>(stoi(number)));
        }

        isNumber = false;
        isReal = false;
        number.clear();
    }
}
Stack Calculator::parseString(const std::string &str)
{
    char character;

    isNumber = false;
    isReal = false;
    number.clear();

    opStack.clear();
    outputStack.clear();


    for(uint i=0;i<str.size();i++)
    {
        character = str.at(i);
        if(std::isdigit(character)) {
            isNumber = true;
            number.push_back(character);
        } else if(character == DOT || character == COMMA) {
            isReal = true;
            number.push_back(DOT);
        } else {
            processNumber();

            if(character == LEFT_PARENTHESIS) {
                opStack.push_back(std::make_shared<Operation>(LEFT_PARENTHESIS));
            } else if(character == RIGHT_PARENTHESIS) {
                bool stop = false;
                while(!stop && opStack.size() > 0)
                {
                    ElementPtr item = opStack.back();
                    opStack.pop_back();

                    if(item.get()->type() == Types::OPERATION) {
                        Operation *op = dynamic_cast<Operation*>(item.get());
                        if(op->getValue() != Operations::LPARENTHESIS) {
                            outputStack.push_back(item);
                        } else {
                            stop = true;
                        }
                    }
                }
            } else if(Operation::isOperation(character)) {
                std::shared_ptr<Operation> op = std::make_shared<Operation>(character);

                if(opStack.size() > 0) {
                    bool stop = false;

                    while(!stop && opStack.size() > 0) {
                        ElementPtr item = opStack.back();

                        if(item.get()->type() == Types::OPERATION) {
                            Operation *sop = dynamic_cast<Operation*>(item.get());

                            if(sop->getValue() != Operations::LPARENTHESIS && sop->prio() >= op->prio()) {
                                opStack.pop_back();
                                outputStack.push_back(item);
                            } else {
                                stop = true;
                            }
                        }
                    }

                    opStack.push_back(op);
                } else {
                    opStack.push_back(op);
                }
            }
        }
    }

    processNumber();

    while(opStack.size() > 0) {
        outputStack.push_back(opStack.back());
        opStack.pop_back();
    }

    return outputStack;
}

ElementPtr Calculator::applyOperation(ElementPtr left, ElementPtr right, ElementPtr operation)
{
    bool bothInteger = false;
    ElementPtr result;

    if(left->type() == Types::DOUBLE || right->type() == Types::DOUBLE)
    {
        if(left->type() == Types::INTEGER)
        {
            left = std::make_shared<Double>(static_cast<Integer*>(left.get())->getValue());
        }

        if(right->type() == Types::INTEGER)
        {
            right = std::make_shared<Double>(static_cast<Integer*>(right.get())->getValue());
        }

        result = std::make_shared<Double>(0);
    }
    else
    {
        result = std::make_shared<Integer>(0);
        bothInteger = true;
    }

    Operation* op = static_cast<Operation*>(operation.get());
    switch(op->getValue()) {
    case Operations::ADD:
        if(bothInteger) {
            Integer *leftInt = static_cast<Integer*>(left.get());
            Integer *rightInt = static_cast<Integer*>(right.get());
            Integer *res = static_cast<Integer*>(result.get());
            res->setValue(leftInt->getValue() + rightInt->getValue());
        } else {
            Double *leftDbl = static_cast<Double*>(left.get());
            Double *rightDbl = static_cast<Double*>(right.get());
            Double *res = static_cast<Double*>(result.get());
            res->setValue(leftDbl->getValue() + rightDbl->getValue());
        }
        break;
    case Operations::SUB:
        if(bothInteger) {
            Integer *leftInt = static_cast<Integer*>(left.get());
            Integer *rightInt = static_cast<Integer*>(right.get());
            Integer *res = static_cast<Integer*>(result.get());
            res->setValue(leftInt->getValue() - rightInt->getValue());
        } else {
            Double *leftDbl = static_cast<Double*>(left.get());
            Double *rightDbl = static_cast<Double*>(right.get());
            Double *res = static_cast<Double*>(result.get());
            res->setValue(leftDbl->getValue() - rightDbl->getValue());
        }
        break;
    case Operations::DIV:
        if(bothInteger) {
            Integer *rightInt = static_cast<Integer*>(right.get());

            if(rightInt->getValue() == 0)
            {
                return std::make_shared<Error>(Errors::DIVBYZERO);
            }
            else
            {
                Integer *leftInt = static_cast<Integer*>(left.get());

                if(leftInt->getValue() % rightInt->getValue() == 0)
                {
                    Integer *res = static_cast<Integer*>(result.get());
                    res->setValue(leftInt->getValue() / rightInt->getValue());
                }
                else
                {
                    double leftV(leftInt->getValue()), rightV(rightInt->getValue());
                    result = std::make_shared<Double>(leftV / rightV);
                    Double *res = static_cast<Double*>(result.get());
                    res->setValue(leftV / rightV);
                }
            }
        } else {
            Double *rightDbl = static_cast<Double*>(right.get());

            if(rightDbl->getValue() == 0)
            {
                return std::make_shared<Error>(Errors::DIVBYZERO);
            }
            else
            {
                Double *leftDbl = static_cast<Double*>(left.get());
                Double *res = static_cast<Double*>(result.get());
                res->setValue(leftDbl->getValue() / rightDbl->getValue());
            }
        }
        break;
    case Operations::MUL:
        if(bothInteger) {
            Integer *leftInt = static_cast<Integer*>(left.get());
            Integer *rightInt = static_cast<Integer*>(right.get());
            Integer *res = static_cast<Integer*>(result.get());
            res->setValue(leftInt->getValue() * rightInt->getValue());
        } else {
            Double *leftDbl = static_cast<Double*>(left.get());
            Double *rightDbl = static_cast<Double*>(right.get());
            Double *res = static_cast<Double*>(result.get());
            res->setValue(leftDbl->getValue() * rightDbl->getValue());
        }
        break;
    case Operations::POW:
        if(bothInteger) {
            Integer *leftInt = static_cast<Integer*>(left.get());
            Integer *rightInt = static_cast<Integer*>(right.get());

            if(leftInt->getValue() == 0 && rightInt->getValue() == 0)
            {
                return std::make_shared<Error>(Errors::ZEROINZEROPOW);
            } else {
                Integer *res = static_cast<Integer*>(result.get());
                res->setValue(static_cast<int>(pow(leftInt->getValue(), rightInt->getValue())));
            }
        } else {
            Double *leftDbl = static_cast<Double*>(left.get());
            Double *rightDbl = static_cast<Double*>(right.get());
            if(leftDbl->getValue() == 0 && rightDbl->getValue() == 0)
            {
                return std::make_shared<Error>(Errors::ZEROINZEROPOW);
            } else {
                Double *res = static_cast<Double*>(result.get());
                res->setValue(pow(leftDbl->getValue(), rightDbl->getValue()));
            }
        }
        break;
    default:
        return std::make_shared<Error>(Errors::INVOPER);
    }

    return result;
}

ElementPtr Calculator::calculateExpression(Stack input)
{
    Stack buffer;

    while(input.size() > 0)
    {
        ElementPtr el = input.front();
        input.pop_front();

        if(el->type() == Types::OPERATION) {
            if(buffer.size() < 2) {
                return std::make_shared<Error>(Errors::INVEXPR);
            } else {
                auto right = buffer.back();
                buffer.pop_back();
                auto left = buffer.back();
                buffer.pop_back();

                ElementPtr opRes = applyOperation(left, right, el);

                if(opRes->type() == Types::ERROR)
                {
                    buffer.clear();
                    buffer.push_back(opRes);
                    break;
                }
                buffer.push_back(opRes);
            }
        } else {
            buffer.push_back(el);
        }
    }

    if(buffer.size() == 1) {
        return buffer.back();
    } else {
        return std::make_shared<Error>(Errors::INVEXPR);
    }
}

ElementPtr Calculator::calculateExpression(const std::string &str)
{
    return calculateExpression(parseString(str));
}
