#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <string>

#include "types.h"

class Calculator
{
protected:
    bool isNumber, isReal;
    std::string number;

    Stack opStack, outputStack;

    void processNumber();
    ElementPtr applyOperation(ElementPtr left, ElementPtr right, ElementPtr operation);

public:
    Calculator();

    Stack parseString(const std::string &str);
    ElementPtr calculateExpression(Stack input);
    ElementPtr calculateExpression(const std::string &str);
};

#endif // CALCULATOR_H
