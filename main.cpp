#include <iomanip>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "calculator.h"

static std::mutex mtx;

void calculateThread(std::string expression, int threadNum)
{
    ElementPtr result = Calculator().calculateExpression(expression);
    std::lock_guard<std::mutex> lock(mtx);
    std::cout << threadNum << ";";

    if(result->type() == Types::ERROR) {
        std::cout << static_cast<Error*>(result.get())->toString();
    } else if(result->type() == Types::DOUBLE) {
        std::cout << std::setprecision(3) << static_cast<Double*>(result.get())->getValue();
    } else if(result->type() == Types::INTEGER) {
        std::cout << static_cast<Integer*>(result.get())->getValue() << ".00";
    }

    std::cout << std::endl;
}

int main()
{
    std::vector<std::thread> threads;
    int expressionsCount = 0, threadNum = 0;
    for (std::string line; std::getline(std::cin, line) && threadNum <= expressionsCount;)
    {
        if(threadNum == 0) {
            expressionsCount = std::stoi(line);
            mtx.lock();
            std::cout << expressionsCount << std::endl;
            mtx.unlock();
        } else {
            threads.push_back(std::thread(calculateThread, line, threadNum));
        }
        threadNum++;
    }

    for(unsigned int i = 0; i < threads.size(); i++)
    {
        threads.at(i).join();
    }

    return 0;
}
