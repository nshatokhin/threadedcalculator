#ifndef TYPES_H
#define TYPES_H

#include <deque>
#include <map>
#include <memory>
#include <unordered_map>
#include <cstring>

enum Types {
    ERROR,
    OPERATION,
    INTEGER,
    DOUBLE
};

class Element {
protected:
    Types type_;
    void *value_;

public:
    Element(const Types &type = Types::OPERATION);
    virtual ~Element();

    Types type() const;

    void setType(const Types &type);

    virtual void * value() const = 0;

    virtual void setValue(void * value) = 0;
};

enum Errors {
    DIVBYZERO,
    ZEROINZEROPOW,
    INVEXPR,
    INVOPER
};

typedef std::map<Errors, std::string> ErrToString;

class Error : public Element {
protected:
    static const ErrToString errToString_;

public:
    Error(const Errors &value = Errors::DIVBYZERO);

    virtual ~Error();

    Errors getValue() const;

    void setValue(int value);

    Errors* getValuePtr() const;

    virtual void * value() const;

    virtual void setValue(void * value);

    std::string toString() const;
};

enum Operations {
    ADD,
    SUB,
    DIV,
    MUL,
    POW,
    LPARENTHESIS,
    RPARENTHESIS
};

typedef std::map<Operations, char> OpPrio;
typedef std::map<char, Operations> OpCaster;

class Operation : public Element {
protected:
    static const OpPrio prioTable_;
    static const OpCaster operationCaster_;

public:
    Operation(const Operations &value = Operations::ADD);
    Operation(const char &value);

    virtual ~Operation();

    static const OpPrio prioTable();
    static const OpCaster operationCaster();

    static bool isOperation(const char &character);

    static Operation castOperation(const char &character);

    static int prio(const Operations& op);

    Operations getValue() const;

    Operations* getValuePtr() const;

    virtual void * value() const;

    virtual void setValue(void * value);

    int prio() const;
};

class Integer : public Element {

public:
    Integer(const int &value = 0);

    virtual ~Integer();

    int getValue() const;

    void setValue(int value);

    int* getValuePtr() const;

    virtual void * value() const;

    virtual void setValue(void * value);
};

class Double : public Element {

public:
    Double(const double &value = 0);

    virtual ~Double();

    double getValue() const;

    void setValue(double value);

    double* getValuePtr() const;

    virtual void * value() const;

    virtual void setValue(void * value);
};

typedef std::shared_ptr<Element> ElementPtr;
typedef std::deque<ElementPtr> Stack;

#endif // TYPES_H
