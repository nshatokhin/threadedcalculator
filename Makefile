LINK          = g++
CXX           = g++
CXXFLAGS      = -pipe -g -std=gnu++11 -Wall -W -fPIC $(DEFINES)
INCPATH       = -I.

OBJECTS       = main.o \
		types.o \
		calculator.o

TARGET        = calculator

LIBS          = -pthread 


%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCPATH) -c $<

$(TARGET):  $(OBJECTS)  
	$(LINK) -o $(TARGET) $(OBJECTS) $(LIBS)

all: $(TARGET)

clean:
	rm -rf $(TARGET)
	rm -rf *.o
